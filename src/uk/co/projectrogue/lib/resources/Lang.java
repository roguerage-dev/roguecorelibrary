package uk.co.projectrogue.lib.resources;

import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;

public enum Lang {
    INVALID_COMMAND("invalid.command", "&cInvalid Command."),
    INVALID_SUBCOMMAND("invalid.subcommand", "&cInvalid Sub-Command."),
    INVALID_PERMISSION("invalid.permission", "&cYou need the ({0}) permission to perform that command."),
    INVALID_NUMBER("invalid.number", "&cInvalid number: {0}"),
    INVALID_ARGUMENTS("invalid.arguments", "&cInvalid Arguments."),
    INVALID_ARRAY("invalid.array", "&cThere are no items in the selected list: {0}"),
    INVALID_SENDER("invalid.sender", "&cInvalid Command Sender: {0}"),

    /**
     * PLUGIN NOTIFICATIONS
     **/
    PLUGIN_REGISTERED_COMMAND("plugin.registered.command", "Command '{0}' has been registered."),

    PLUGIN_RELOADED("plugin.reloaded", "{0} has been reloaded."),
    PLUGIN_RELOADING("plugin.reloading", "{0} is reloading..."),

    PLUGIN_ENABLED("plugin.enabled", "{0} version {1} has been enabled."),
    PLUGIN_ENABLING("plugin.enabling", "Enabling {0} version {1}..."),
    /**
     * PLUGIN NOTIFICATIONS
     **/

    PREFIX("plugin.prefix", "&4[&6RCL&r&4] &r");

    private static FileConfiguration LANG;
    private String path, def;

    Lang(final String path, final String start) {
        this.path = path;
        this.def = start;
    }

    public static void setFile(final FileConfiguration config) {
        LANG = config;
    }

    public String getDefault() {
        return this.def;
    }

    public String getPath() {
        return this.path;
    }

    public String getConfigValue(final Object... args) {
        String value = ChatColor.translateAlternateColorCodes('&', LANG.getString(this.path, this.def));

        if (args == null) {
            return value;
        } else {
            if (args.length == 0) {
                return value;
            }

            for (int i = 0; i < args.length; i++) {
                value = value.replace("{" + i + "}", args[i].toString());
            }
        }

        return value;
    }
}
