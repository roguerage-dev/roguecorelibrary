package uk.co.projectrogue.lib.resources;

import uk.co.projectrogue.shared.Configuration;

import java.util.Arrays;

public enum Config {
    METRICS_ENABLED("config.yml", "metrics.enabled", false, true),

    UPDATER_ENABLED("config.yml", "updater.enabled", false, true),
    UPDATER_INTERVAL("config.yml", "updater.interval", false, "1h"),

    REMOVED_COMMANDS("commands.yml", "disabled-commands", false, Arrays.asList("command1", "command2")),

    LANGUAGE("config.yml", "language", false, "en-gb"),
	/* END ENUM */;

    private final String file;
    private final String path;
    private final Boolean force;
    private final Object def;

    Config(final String file, final String path, final Boolean force, final Object def) {
        // Define file variable...
        this.file = file;

        // Define path variable...
        this.path = path;

        // Define force variable...
        this.force = force;

        // Define def variable...
        this.def = def;
    }

    public static void setDefaults(final Configuration configFile) {
        // Get the FileName
        final String fileName = configFile.getFileName();

        // Iterate through all Values
        for (final Config config : values()) {
            // Check whether File matches Config File
            if (!config.getFile().equalsIgnoreCase(fileName)) {
                // Continue
                continue;
            }

            // Check the Forceful Settings
            if (config.isForced()) {
                // Set the Node
                configFile.set(config.getPath(), config.getDefault());

                // Continue
                continue;
            }

            // Check if Path Exists
            if (configFile.get(config.getPath()) != null) {
                // Continue
                continue;
            }

            // Set the Node
            configFile.set(config.getPath(), config.getDefault());
        }

        // Save the Config
        configFile.save();
    }

    public String getFile() {
        // Return variable...
        return file;
    }

    public String getPath() {
        // Return variable...
        return path;
    }

    public Object getDefault() {
        // Return variable...
        return def;
    }

    private Boolean isForced() {
        // Return variable...
        return force;
    }
}