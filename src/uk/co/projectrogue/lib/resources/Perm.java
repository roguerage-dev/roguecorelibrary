package uk.co.projectrogue.lib.resources;

public enum Perm {
    PERMISSION_COMMAND_BASE("roguecorelibrary.command.base"),
    PERMISSION_COMMAND_PLUGINS("roguecorelibrary.command.plugins"),
    PERMISSION_COMMAND_UPDATE("roguecorelibrary.command.update"),
    PERMISSION_COMMAND_RELOAD("roguecorelibrary.command.reload"),
    /* END ENUM */;

    private String perm;

    Perm(final String perm) {
        this.perm = perm;
    }

    public String getPermissionNode() {
        return this.perm;
    }
}
