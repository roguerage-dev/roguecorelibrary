package uk.co.projectrogue.lib;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;
import org.mcupdate.MCUpdateStats;
import uk.co.projectrogue.lib.handlers.CommandHandler;
import uk.co.projectrogue.lib.handlers.ConfigHandler;
import uk.co.projectrogue.lib.handlers.LanguageHandler;
import uk.co.projectrogue.lib.handlers.PluginHandler;
import uk.co.projectrogue.lib.tasks.MetricsTask;
import uk.co.projectrogue.lib.tasks.UpdaterTask;
import uk.co.projectrogue.lib.util.Commands;
import uk.co.projectrogue.shared.Configuration;

public class RogueCoreLibrary extends JavaPlugin {
	private static RogueCoreLibrary library;
	public static RogueCoreLibrary getLibrary() {
		return library;
	}

	private ConfigHandler configHandler;
	private CommandHandler commandHandler;
	private PluginHandler pluginHandler;
	private LanguageHandler languageHandler;

	public void sendServerMessage(final String message) {
		getLogger().info(message);
	}

	public void sendPlayerMessage(Player player, String message) {
		final String msg = color(message);

		player.sendMessage(msg);
	}

	public String color(final String message) {
		return ChatColor.translateAlternateColorCodes('&', message);
	}

	public void onEnable() {
		library = this;

		// Alert the Server
		sendServerMessage("==================================");
		sendServerMessage( "Running " + getDescription().getName() + " v" + getDescription().getVersion() );
		sendServerMessage("==================================");
		sendServerMessage( "Please Note:" );
		sendServerMessage( "Running this plugin disables a few server commands that would" );
		sendServerMessage( "harm the operational standpoint of various plugins that utilize" );
		sendServerMessage( "the Core plugin features." );
		sendServerMessage("==================================");

		// ConfigHandler - Handles Configuration Files
		setConfigHandler(new ConfigHandler(this, new Configuration(this, "config.yml", null, "Config: RogueCoreLibrary")));

		// LangHandler - Handles Language Files
		setLanguageHandler(new LanguageHandler(this));
		getLanguageHandler().createNewFile();

		setPluginHandler(new PluginHandler(this));

		try {
			Commands.removeCommands("rl", "bukkit:rl", "spigot:rl", "reload", "bukkit:reload", "spigot:reload");
		} catch (Exception e) {
			e.printStackTrace();
		}

		// Add Commands
		setCommandHandler(new CommandHandler(this));
		getCommand("rcl").setExecutor( getCommandHandler() );

		initTasker();

		sendServerMessage("==================================");
		sendServerMessage(getDescription().getName() + " v" + getDescription().getVersion() + " has been enabled.");

		try {
			// Initialize Metrics - if enabled
			if (getConfigHandler().areMetricsEnabled()) {
				sendServerMessage("==================================");
				sendServerMessage("Initializing MCUpdate Metrics...");

				MCUpdateStats mcUpdateStats = new MCUpdateStats(this);
				mcUpdateStats.setUpdaterEnabled(false);

				// Begin MCUpdateStats Metrics
				mcUpdateStats.start();

				sendServerMessage("MCUpdate Metrics Initialized.");
				sendServerMessage("==================================");
			}
		} catch (final Exception ignored) {}
	}

	private void initTasker() {
		// Create Tasker
		BukkitRunnable tasker = new BukkitRunnable() {
			@Override
			public void run() {
				// Check Metrics are Enabled
				if( getConfigHandler().areMetricsEnabled() ) {
					// Begin Metrics Task
					MetricsTask metricsTask = new MetricsTask(library);
					metricsTask.runTask(library);
				}

				// Check Updater is Enabled
				/*
				if( getConfigHandler().isUpdaterEnabled() ) {
					// Begin Updater Task
					UpdaterTask updaterTask = new UpdaterTask(library);
					updaterTask.runTaskTimer(library, 0, library.getConfigHandler().getUpdaterInterval() * 60 * 20);
				}
				*/
			}
		};

		// Begin Tasker
		tasker.runTaskLaterAsynchronously(this, 20 * 60 * 2);
	}

	public void sendMessage(final CommandSender sender, final String message) {
		sender.sendMessage(color(message));
	}

	public ConfigHandler getConfigHandler() {
		return this.configHandler;
	}

	private void setConfigHandler(final ConfigHandler handler) {
		this.configHandler = handler;
	}

	public CommandHandler getCommandHandler() {
		return this.commandHandler;
	}

	private void setCommandHandler(final CommandHandler handler) {
		this.commandHandler = handler;
	}

	public LanguageHandler getLanguageHandler() {
		return this.languageHandler;
	}

	private void setLanguageHandler(final LanguageHandler handler) {
		this.languageHandler = handler;
	}

	public PluginHandler getPluginHandler() {
		return this.pluginHandler;
	}

	private void setPluginHandler(final PluginHandler handler) {
		this.pluginHandler = handler;
	}

	public boolean hook(Plugin plugin) {
		return false;
	}
}