package uk.co.projectrogue.lib.handlers;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import uk.co.projectrogue.lib.RogueCoreLibrary;
import uk.co.projectrogue.lib.resources.Lang;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.logging.Level;

public class LanguageHandler {
    private final RogueCoreLibrary plugin;

    private FileConfiguration langConfig;
    private File langConfigFile;
    private String langFile = "";

    public LanguageHandler(final RogueCoreLibrary plugin) {
        this.plugin = plugin;
        this.langFile = this.plugin.getConfigHandler().getLanguage() + ".yml";
    }

    public void createNewFile() {
        reloadConfig();
        saveConfig();

        Lang.setFile(langConfig);

        loadConfig();

        plugin.sendServerMessage("Language file loaded (" + langFile + ")");
    }

    public FileConfiguration getConfig() {
        if (langConfig == null) {
            this.reloadConfig();
        }

        return langConfig;
    }

    public void loadConfig() {
        langConfig.options().header("Language file");

        for (final Lang value : Lang.values()) {
            langConfig.addDefault(value.getPath(), value.getDefault());
        }

        langConfig.options().copyDefaults(true);
        saveConfig();
    }

    @SuppressWarnings("deprecation")
    public void reloadConfig() {
        if (langConfigFile == null) {
            langConfigFile = new File(plugin.getDataFolder() + "/lang", langFile);
        }

        langConfig = YamlConfiguration.loadConfiguration(langConfigFile);

        // Look for defaults in the jar
        final InputStream defConfigStream = plugin.getResource(langFile);
        if (defConfigStream != null) {
            final YamlConfiguration defConfig = YamlConfiguration.loadConfiguration(defConfigStream);
            langConfig.setDefaults(defConfig);
        }
    }

    public void saveConfig() {
        if (langConfig == null || langConfigFile == null) {
            return;
        }

        try {
            getConfig().save(langConfigFile);
        } catch (final IOException ex) {
            plugin.getLogger().log(Level.SEVERE, "Could not save config to " + langConfigFile, ex);
        }
    }
}
