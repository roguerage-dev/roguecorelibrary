package uk.co.projectrogue.lib.handlers;

import org.bukkit.plugin.Plugin;
import uk.co.projectrogue.lib.RogueCoreLibrary;

import java.util.*;

public class PluginHandler {
    private final RogueCoreLibrary plugin;

    private List<Plugin> plugins = new ArrayList<>();
    private Map<String, Integer> resources = new HashMap<>();

    public PluginHandler(final RogueCoreLibrary plugin) {
        this.plugin = plugin;
    }

    public boolean hook( Plugin core, int resource ) {
        return addPlugin( core, resource );
    }

    private boolean addPlugin(final Plugin core, final int resource) {
        if( plugins.contains( core ) ) return true;

        String coreN = core.getDescription().getName();
        resources.put(coreN, resource);

        plugins.add(core);

        return plugins.contains(core);
    }

    public List<Plugin> getPlugins() {
        return this.plugins;
    }

    public Map<String, Integer> getResources() {
        return this.resources;
    }

    public Plugin getPlugin(String resourceN) {
        return plugin.getServer().getPluginManager().getPlugin(resourceN);
    }
}