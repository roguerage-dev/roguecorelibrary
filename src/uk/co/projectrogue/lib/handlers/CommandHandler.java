package uk.co.projectrogue.lib.handlers;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabExecutor;
import org.bukkit.entity.Player;
import uk.co.projectrogue.lib.RogueCoreLibrary;
import uk.co.projectrogue.lib.commands.*;
import uk.co.projectrogue.lib.commands.base.CommandBase;
import uk.co.projectrogue.lib.commands.subcommands.base.SubCommandBase;
import uk.co.projectrogue.lib.resources.Lang;
import uk.co.projectrogue.shared.resources.Tools;

import java.util.*;
import java.util.Map.Entry;

public class CommandHandler implements TabExecutor {
    private final RogueCoreLibrary plugin;
    private final HashMap<List<String>, CommandBase> commandList = new HashMap<>();

    public CommandHandler(final RogueCoreLibrary plugin) {
        // Define Plugin Object
        this.plugin = plugin;

        // Initialise Commands
        initCommands();
    }

    private void initCommands() {
        addCommand(Arrays.asList("plugins", "pl"), new CommandPlugins(plugin));
        addCommand(Collections.singletonList("update"), new CommandUpdate(plugin));
        addCommand(Arrays.asList("reload", "rl", "restart"), new CommandReload(plugin));
    }

    @Override
    public boolean onCommand(final CommandSender sender, final Command command, final String label, final String[] args) {
        if (args.length == 0) {
            sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&4=================================="));
            sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&fDeveloped by: &6" + plugin.getDescription().getAuthors()));
            sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&fVersion: &6" + plugin.getDescription().getVersion()));
            sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&fWebsite: &6" + plugin.getDescription().getWebsite()));
            sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&fDescription: &6" + plugin.getDescription().getDescription()));
            sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&4=================================="));

            return true;
        }

        final String action = args[0];

        List<String> suggestions = new ArrayList<>();
        List<String> bestSuggestions = new ArrayList<>();

        for (final Entry<List<String>, CommandBase> entry : commandList.entrySet()) {
            String suggestion = Tools.findClosestSuggestion(action, entry.getKey());

            if (suggestion != null) {
                suggestions.add(suggestion);
            }

            for (final String actionString : entry.getKey()) {
                if (actionString.equalsIgnoreCase(action)) {
                    if( args.length > 1 ) {
                        final String subAction = args[1];

                        for (final Entry<List<String>, SubCommandBase> subEntry : entry.getValue().getSubCommands().entrySet()) {
                            String subSuggestion = Tools.findClosestSuggestion(action, subEntry.getKey());

                            if (suggestion != null) {
                                suggestions.add(subSuggestion);
                            }

                            for (final String subActionString : subEntry.getKey()) {
                                if (subActionString.equalsIgnoreCase(subAction)) {
                                    return subEntry.getValue().onCommand(sender, command, label, args);
                                }
                            }
                        }
                    }

                    return entry.getValue().onCommand(sender, command, label, args);
                }
            }
        }

        for (String suggestion : suggestions) {
            String[] split = suggestion.split(";");

            int editDistance = Integer.parseInt(split[1]);

            if (editDistance <= 2) {
                bestSuggestions.add(split[0]);
            }
        }

        sender.sendMessage(Lang.PREFIX.getConfigValue() + Lang.INVALID_COMMAND.getConfigValue());

        return true;
    }

    @Override
    public List<String> onTabComplete(final CommandSender sender, final Command command, final String label, final String[] args) {
        if (args.length <= 1) {
            // Show a list of commands, if needed...
            final List<String> commands = new ArrayList<>();

            for (final Entry<List<String>, CommandBase> entry : commandList.entrySet()) {
                final List<String> list = entry.getKey();

                commands.add(list.get(0));
            }

            // Return
            return commands;
        }

        // Return Sub-Command
        final String subCommand = args[0].trim();

        // Return on-tab-complete of sub-command
        for (final Entry<List<String>, CommandBase> entry : commandList.entrySet()) {
            for (final String alias : entry.getKey()) {
                if (subCommand.trim().equalsIgnoreCase(alias)) {
                    return entry.getValue().onTabComplete(sender, command, label, args);
                }
            }
        }

        // Return NULL
        return null;
    }

    public void addCommand(final List<String> commands, final CommandBase handler) {
        // Put Commands and Handlers to List
        commandList.put(commands, handler);
    }

    public boolean hasPermission(final String permission, final CommandSender sender) {
        if (!sender.hasPermission(permission)) {
            sender.sendMessage(Lang.PREFIX.getConfigValue() + Lang.INVALID_PERMISSION.getConfigValue(permission));
            return false;
        }

        return true;
    }

    public boolean isSenderPlayer(CommandSender sender) {
        if(!(sender instanceof Player)) {
            sender.sendMessage(Lang.PREFIX.getConfigValue() + Lang.INVALID_SENDER.getConfigValue(sender.getName()));
            return false;
        }

        return true;
    }

    public HashMap<List<String>, CommandBase> getCommands() {
        return commandList;
    }
}