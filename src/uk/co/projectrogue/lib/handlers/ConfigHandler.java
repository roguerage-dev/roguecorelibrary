package uk.co.projectrogue.lib.handlers;

import uk.co.projectrogue.lib.RogueCoreLibrary;
import uk.co.projectrogue.lib.resources.Config;
import uk.co.projectrogue.shared.Configuration;
import uk.co.projectrogue.shared.resources.Tools;
import uk.co.projectrogue.shared.utils.TimeUtils;

public class ConfigHandler {
    private final RogueCoreLibrary plugin;
    private final Configuration config;

    public ConfigHandler(final RogueCoreLibrary plugin, final Configuration config) {
        this.plugin = plugin;

        Config.setDefaults(config);

        this.config = config;
    }

    /**
     * Returns the Configuration File actively loaded for the handler.
     */
    private Configuration getConfig() {
        return this.config;
    }

    public String getLanguage() {
        return getConfig().getString(Config.LANGUAGE.getPath(), (String) Config.LANGUAGE.getDefault());
    }

    public boolean areMetricsEnabled() {
        return getConfig().getBoolean(Config.METRICS_ENABLED.getPath(), (Boolean) Config.METRICS_ENABLED.getDefault());
    }

    public boolean isUpdaterEnabled() {
        return getConfig().getBoolean(Config.UPDATER_ENABLED.getPath(), (Boolean) Config.UPDATER_ENABLED.getDefault());
    }

    public int getUpdaterInterval() {
        String interval = getConfig().getString(Config.UPDATER_INTERVAL.getPath(), (String) Config.UPDATER_INTERVAL.getDefault());
        int value;

        if( interval.contains("h") || interval.contains("m") || interval.contains("s") ) {
            value = TimeUtils.stringToTime(interval, TimeUtils.Time.MINUTES);
        } else {
            value = Tools.stringToInt(interval);
        }

        return value;
    }
}