package uk.co.projectrogue.lib.tasks;

import org.bukkit.plugin.Plugin;
import org.mcupdate.MCUpdateStats;
import uk.co.projectrogue.lib.RogueCoreLibrary;

import java.util.List;

public class MetricsTask extends Task {
    public MetricsTask(final RogueCoreLibrary plugin) {
        super(plugin);

        setName("Metrics");
    }

    @Override
    public void run() {
        // Define a List of Plugins
        List<Plugin> pluginList = plugin.getPluginHandler().getPlugins();

        // Iterate through Available Plugins
        for( Plugin core : pluginList ) {
            // Attempt to begin Metrics
            try {
                // Define Metrics Object
                MCUpdateStats metrics = new MCUpdateStats(core);
                metrics.setUpdaterEnabled(false);

                // Begin Metrics
                metrics.start();

                // Notify Server
                notify("==================================");
                notify("MCUpdate metrics enabled for " + core.getDescription().getName());
                notify("==================================");
            } catch (final Exception ignored) {
                // Ignore Exception
            }
        }
    }
}