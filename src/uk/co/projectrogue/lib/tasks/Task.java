package uk.co.projectrogue.lib.tasks;

import org.bukkit.scheduler.BukkitRunnable;
import uk.co.projectrogue.lib.RogueCoreLibrary;

public abstract class Task extends BukkitRunnable {
    final RogueCoreLibrary plugin;
    private String taskName;

    Task(final RogueCoreLibrary plugin) {
        this.plugin = plugin;
    }

    void notify(String message) {
        plugin.sendServerMessage("[Task] -> [" + getName() + "] " + message);
    }
    
    protected void setName(String name) {
        this.taskName = name;
    }

    public String getName() {
        return this.taskName;
    }
}