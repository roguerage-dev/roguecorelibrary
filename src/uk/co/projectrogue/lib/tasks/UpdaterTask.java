package uk.co.projectrogue.lib.tasks;

import org.bukkit.plugin.Plugin;
import uk.co.projectrogue.lib.RogueCoreLibrary;

import java.util.*;

public class UpdaterTask extends Task {
    public UpdaterTask(final RogueCoreLibrary plugin) {
        super(plugin);

        setName("Updater");
    }

    @Override
    public void run() {
        // Define Resource Map
        Map<String, Integer> resources = plugin.getPluginHandler().getResources();

        // Iterate through Resources
        for( String pluginN : resources.keySet() ) {
            try {
                // Notify Server
                notify("==================================");
                notify("Checking for updates for " + pluginN);
                notify("==================================");

                // Define Plugin
                Plugin pluginP = plugin.getServer().getPluginManager().getPlugin(pluginN);

                // Define Plugin ID
                int pluginI = resources.get(pluginN);

                // Define Plugin Version
                String pluginV = pluginP.getDescription().getVersion();
            } catch (final Exception exception) {
                notify("============================");
                notify("An exception occurred during Resource Update Check.");
                notify("Resource: " + pluginN);
                notify("Error Message: " + exception.getMessage());
                notify("============================");
                notify("== DETAILED ERROR MESSAGE ==");
                notify("============================");
                exception.printStackTrace();
                notify("============================");
            }
        }
    }
}