package uk.co.projectrogue.lib.commands.subcommands.base;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabExecutor;
import uk.co.projectrogue.lib.RogueCoreLibrary;
import uk.co.projectrogue.lib.resources.Perm;

import java.util.List;

public abstract class SubCommandBase implements TabExecutor {
    protected final RogueCoreLibrary plugin;

    public SubCommandBase(RogueCoreLibrary plugin) {
        this.plugin = plugin;
    }

    private String permission = Perm.PERMISSION_COMMAND_BASE.getPermissionNode();
    private String usage = "plugin usage";
    private String desc = "basic description";

    public String getDescription() {
        return this.desc;
    }

    public void setDescription(String desc) {
        this.desc = desc;
    }

    public String getPermission() {
        return this.permission;
    }

    public void setPermission(String perm) {
        this.permission = perm;
    }

    public String getUsage() {
        return this.usage;
    }

    public void setUsage(String usage) {
        this.usage = usage;
    }

    public String getUsage(String label) {
        return this.usage.replace("<command>", label);
    }

    @Override
    public abstract boolean onCommand(CommandSender sender, Command command, String label, String[] args);

    @Override
    public List<String> onTabComplete(CommandSender sender, Command command, String label, String[] args) {
        return null;
    }
}
