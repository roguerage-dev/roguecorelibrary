package uk.co.projectrogue.lib.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import uk.co.projectrogue.lib.RogueCoreLibrary;
import uk.co.projectrogue.lib.commands.base.CommandBase;
import uk.co.projectrogue.lib.resources.Perm;

import java.util.ArrayList;
import java.util.List;

public class CommandReload extends CommandBase {
    public CommandReload(final RogueCoreLibrary plugin) {
        super(plugin);

        // Set Variables
        setUsage("/<command> plugins");
        setDescription("Neatly lists the available plugins.");
        setPermission(Perm.PERMISSION_COMMAND_RELOAD.getPermissionNode());
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (!plugin.getCommandHandler().hasPermission(Perm.PERMISSION_COMMAND_RELOAD.getPermissionNode(), sender)) {
            return true;
        }



        return true;
    }
}
