package uk.co.projectrogue.lib.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import uk.co.projectrogue.lib.RogueCoreLibrary;
import uk.co.projectrogue.lib.commands.base.CommandBase;
import uk.co.projectrogue.lib.resources.Perm;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class CommandPlugins extends CommandBase {
    public CommandPlugins(final RogueCoreLibrary plugin) {
        super(plugin);

        // Set Variables
        setUsage("/<command> plugins");
        setDescription("Neatly lists the available plugins.");
        setPermission(Perm.PERMISSION_COMMAND_PLUGINS.getPermissionNode());
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (!plugin.getCommandHandler().hasPermission(Perm.PERMISSION_COMMAND_PLUGINS.getPermissionNode(), sender)) {
            return true;
        }

        double selection = 30D;
        if( sender instanceof Player ) selection = 6D;

        if( args.length == 1 ) {
            showPages( sender, 1, selection );
        } else {
            int pageIndex = 1;
            String value = args[1];

            try {
                pageIndex = Integer.parseInt( value );
            } catch( final Exception exception ) {
                plugin.sendMessage(sender, "&cInvalid number: " + value);
            }

            showPages( sender, pageIndex, selection );
        }

        return true;
    }

    private void showPages(CommandSender sender, int pageIndex, double selection) {
        /// Define Message List
        List<String> messages = new ArrayList<>();

        List<Plugin> plugins = new ArrayList<>();
        Collections.addAll(plugins, plugin.getServer().getPluginManager().getPlugins());

        // Define Variables
        int pageI = pageIndex;

        final int pageM = (int) Math.ceil( plugins.size() / selection );

        if( pageI > pageM || pageI == 0 ) pageI = pageM;

        int pageS = 0;
        int pageE = (int) selection;

        if( pageI != 1 ) {
            final int pageD = pageI - 1;

            pageS += ((int) selection) * pageD;
            pageE = pageS + ((int) selection);
        }

        messages.add("&4===================");
        messages.add("&6[ Current Plugins ]");
        messages.add("&4===================");
        for( int i = pageS; i < pageE; i++ ) {
            if( i >= plugins.size() ) break;

            Plugin plug = plugins.get(i);

            String pluginN = plug.getDescription().getName();
            String pluginV = plug.getDescription().getVersion();
            String pluginI = String.format( "%03d", i + 1 );
            String pluginE = Boolean.toString( plug.isEnabled() );

            messages.add("&4[&f&l" + pluginI + "&r&4] &2" + pluginN + " &f&lv" + pluginV + " &6[" + pluginE + "]");
        }
        messages.add("&4===================");
        messages.add("&6[ Page: " + pageI + " of " + pageM + " ]" );
        messages.add("&4===================");

        // Submit Messages
        for( String message : messages ) {
            plugin.sendMessage( sender, message );
        }
    }
}
