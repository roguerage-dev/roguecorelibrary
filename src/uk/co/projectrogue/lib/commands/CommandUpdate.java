package uk.co.projectrogue.lib.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import uk.co.projectrogue.lib.RogueCoreLibrary;
import uk.co.projectrogue.lib.commands.base.CommandBase;
import uk.co.projectrogue.lib.resources.Perm;

public class CommandUpdate extends CommandBase {
    public CommandUpdate(final RogueCoreLibrary plugin) {
        super(plugin);

        // Set Variables
        setUsage("/<command> plugins");
        setDescription("Neatly lists the available plugins.");
        setPermission(Perm.PERMISSION_COMMAND_UPDATE.getPermissionNode());
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (!plugin.getCommandHandler().hasPermission(Perm.PERMISSION_COMMAND_UPDATE.getPermissionNode(), sender)) {
            return true;
        }

        return true;
    }
}
