package uk.co.projectrogue.lib.util;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.SimpleCommandMap;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.SimplePluginManager;
import uk.co.projectrogue.lib.RogueCoreLibrary;
import uk.co.projectrogue.rcl.RogueCoreLib;
import uk.co.projectrogue.reflection.RogueReflect;

import java.util.Iterator;
import java.util.Map;

public class Commands {
    private static final RogueCoreLibrary plugin = RogueCoreLibrary.getLibrary();

    @SuppressWarnings("unchecked")
    public static void removeCommands(String... labels ) throws Exception {
        PluginManager pluginManager = Bukkit.getServer().getPluginManager();

        if( pluginManager instanceof SimplePluginManager) {
            SimplePluginManager simplePluginManager = (SimplePluginManager) pluginManager;
            SimpleCommandMap simpleCommandMap = (SimpleCommandMap) RogueReflect.getField(simplePluginManager, SimplePluginManager.class, "commandMap");
            Map<String, Command> knownCommands = (Map<String, Command>) RogueReflect.getField(simpleCommandMap, SimpleCommandMap.class, "knownCommands");
            Iterator<Map.Entry<String, Command>> iterator = knownCommands.entrySet().iterator();

            while (iterator.hasNext()) {
                Map.Entry<String, Command> entry = iterator.next();
                String label = entry.getKey();

                for( String remove : labels ) {
                    if( label.equals(remove) ) {
                        entry.getValue().unregister(simpleCommandMap);
                        iterator.remove();
                        plugin.sendServerMessage("Removed Command: " + remove);
                        break;
                    }
                }
            }
        }
    }
}
