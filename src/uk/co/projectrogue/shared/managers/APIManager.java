package uk.co.projectrogue.shared.managers;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import uk.co.projectrogue.shared.hooks.actionbar.*;
import uk.co.projectrogue.shared.hooks.hologram.*;
import uk.co.projectrogue.shared.hooks.json.items.*;
import uk.co.projectrogue.shared.hooks.json.messages.*;
import uk.co.projectrogue.shared.hooks.particle.*;
import uk.co.projectrogue.shared.hooks.ping.*;
import uk.co.projectrogue.shared.hooks.title.*;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class APIManager {
    private static APIManager manager;
    private static JavaPlugin plugin;

    private JSONMessageAPI jsonMessageAPI;
    private ActionBarAPI actionBarAPI;
    private JSONItemAPI jsonItemAPI;
    private ParticleAPI particleAPI;
    private TitleAPI titleAPI;
    private PingAPI pingAPI;

    public static JavaPlugin getPlugin() {
        return plugin;
    }

    public APIManager(final JavaPlugin plugin) {
        APIManager.plugin = plugin;

        manager = this;
    }

    private void notify(String message) {
        plugin.getLogger().info("[APIManager] " + message);
    }

    public String color(String message) {
        return ChatColor.translateAlternateColorCodes('&', message);
    }

    public static Collection<? extends Player> getOnlinePlayers() {
        return plugin.getServer().getOnlinePlayers();
    }

    public List<String> colorList(List<String> colorList) {
        List<String> newList = new ArrayList<>();
        for( String line : colorList ) {
            newList.add( color(line) );
        }

        return newList;
    }

    private String getVersion() {
        return plugin.getServer().getClass().getPackage().getName().replace(".", ",").split(",")[3];
    }

    public void hook() {
        // Define Start Time
        long startTime = System.currentTimeMillis();

        // Define Server Version
        String serverVersion = getVersion();

        // Iterate through Server Versions
        switch (serverVersion) {
            case "v1_9_R2":
                pingAPI = new PingAPI1_9_R2();
                titleAPI = new TitleAPI1_9_R2();
                particleAPI = new ParticleAPI1_9_R2();
                actionBarAPI = new ActionBarAPI1_9_R2();

                break;
            case "v1_9_R1":
                pingAPI = new PingAPI1_9_R1();
                titleAPI = new TitleAPI1_9_R1();
                particleAPI = new ParticleAPI1_9_R1();
                actionBarAPI = new ActionBarAPI1_9_R1();

                break;
            case "v1_8_R3":
                pingAPI = new PingAPI1_8_R3();
                titleAPI = new TitleAPI1_8_R3();
                particleAPI = new ParticleAPI1_8_R3();
                actionBarAPI = new ActionBarAPI1_8_R3();

                break;
            case "v1_8_R2":
                pingAPI = new PingAPI1_8_R2();
                titleAPI = new TitleAPI1_8_R2();
                particleAPI = new ParticleAPI1_8_R2();
                actionBarAPI = new ActionBarAPI1_8_R2();

                break;
            case "v1_8_R1":
                pingAPI = new PingAPI1_8_R1();
                titleAPI = new TitleAPI1_8_R1();
                particleAPI = new ParticleAPI1_8_R1();
                actionBarAPI = new ActionBarAPI1_8_R1();
            case "v1_7_R4":
                pingAPI = new PingAPI1_7_R4();

                break;
            default:
                // Notify Server
                notify("This server version does not support the APIs provided by " + plugin.getDescription().getName());
                notify("This may be an error, please report it to the developer.");
                notify("Request Support on Discord: https://discord.gg/3RgzDUN");
                notify("Server Version: " + serverVersion);

                break;
        }

        // Define Execution Time
        long execTime = System.currentTimeMillis() - startTime;
        notify("Took " + execTime + "ms to process." );
    }

    //////////////////////////////////////////////////////////////
    // Various Getters								            //
    //////////////////////////////////////////////////////////////

    public HologramAPI getHologramAPI(List<String> lines, double spread, Location location) {
        // Define Server Version
        String serverVersion = getVersion();

        // Iterate through Server Versions
        switch (serverVersion) {
            case "v1_9_R2":
                return new HologramAPI1_9_R2(lines, spread, location);
            case "v1_9_R1":
                return new HologramAPI1_9_R1(lines, spread, location);
            case "v1_8_R3":
                return new HologramAPI1_8_R3(lines, spread, location);
            case "v1_8_R2":
                return new HologramAPI1_8_R2(lines, spread, location);
            case "v1_8_R1":
                return new HologramAPI1_8_R1(lines, spread, location);
            default:
                // Notify Server
                notify("This server version does not support the APIs provided by " + plugin.getDescription().getName());
                notify("This may be an error, please report it to the developer.");
                notify("Request Support on Discord: https://discord.gg/3RgzDUN");
                notify("Server Version: " + serverVersion);

                return null;
        }
    }

    public static APIManager getManager() {
        return manager;
    }

    public JSONMessageAPI getJsonMessageAPI() {
        return this.jsonMessageAPI;
    }

    public JSONItemAPI getJsonItemAPI() {
        return this.jsonItemAPI;
    }

    public ActionBarAPI getActionBarAPI() {
        return this.actionBarAPI;
    }

    public ParticleAPI getParticleAPI() {
        return this.particleAPI;
    }

    public TitleAPI getTitleAPI() {
        return this.titleAPI;
    }

    public PingAPI getPingAPI() {
        return this.pingAPI;
    }
}
