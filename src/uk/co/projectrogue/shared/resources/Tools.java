package uk.co.projectrogue.shared.resources;

import java.util.List;

public class Tools {
    public static String findClosestSuggestion(String input, List<String> list) {
        int lowestDistance = Integer.MAX_VALUE;
        String bestSuggestion = null;

        for (String possibility : list) {
            int dist = editDistance(input, possibility);

            if (dist < lowestDistance) {
                lowestDistance = dist;
                bestSuggestion = possibility;
            }
        }

        return bestSuggestion + ";" + lowestDistance;
    }

    private static int editDistance(String a, String b) {
        a = a.toLowerCase();
        b = b.toLowerCase();

        int[] costs = new int[b.length() + 1];

        for (int j = 0; j < costs.length; j++) {
            costs[j] = j;
        }

        for (int i = 1; i <= a.length(); i++) {
            costs[0] = i;
            int nw = i - 1;

            for (int j = 1; j <= b.length(); j++) {
                int cj = Math.min(1 + Math.min(costs[j], costs[j - 1]), a.charAt(i - 1) == b.charAt(j - 1) ? nw : nw + 1);
                nw = costs[j];
                costs[j] = cj;
            }
        }

        return costs[b.length()];
    }

    public static int stringToInt(final String string) throws NumberFormatException {
        int res = 0;
        if (string != null) res = Integer.parseInt(string);

        return res;
    }

    public static double stringToDouble(final String string) throws NumberFormatException {
        double res = 0;

        if (string != null) res = Double.parseDouble(string);

        return res;
    }
}
