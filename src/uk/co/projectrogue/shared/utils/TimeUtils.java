package uk.co.projectrogue.shared.utils;

import uk.co.projectrogue.shared.resources.Tools;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TimeUtils {
    public enum Time {
        DAYS, HOURS, MINUTES, SECONDS
    }

    public static int stringToTime( String string, final Time time ) {
        int res = 0;

        string = string.trim();

        final Pattern pattern = Pattern.compile("((\\d+)d)?((\\d+)h)?((\\d+)m)?");
        final Matcher matcher = pattern.matcher(string);

        matcher.find();

        final String days = matcher.group(2);
        final String hours = matcher.group(4);
        String minutes = matcher.group(6);

        if( days == null && hours == null && minutes == null ) minutes = string;

        res += Tools.stringToDouble(minutes);
        res += Tools.stringToDouble(hours) * 60;
        res += Tools.stringToDouble(days) * 60 * 24;

        if( time.equals(Time.SECONDS ) ) {
            return res * 60;
        } else if( time.equals(Time.MINUTES) ) {
            return res;
        } else if( time.equals(Time.HOURS) ) {
            return res / 60;
        } else if( time.equals(Time.DAYS) ) {
            return res / 1440;
        } else {
            return 0;
        }
    }
}
