package uk.co.projectrogue.shared.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class VersionUtils {
    private static int[] getNumber(final String version) {
        Pattern pattern = Pattern.compile("(\\d+)\\.(\\d+)(\\.(\\d+))?(\\.(\\d+))?\\s?([a-zA-Z](\\d*))?");
        Matcher matcher = pattern.matcher(version);

        // Throw Exception if Version Match Fails
        if( !matcher.matches() ) throw new IllegalArgumentException("Malformed Version: " + version);

        int maj = Integer.parseInt( matcher.group(1) );
        int min = Integer.parseInt( matcher.group(2) );
        int rev = matcher.group(4) != null ? Integer.parseInt( matcher.group(4) ) : 0;
        int itr = matcher.group(6) != null ? Integer.parseInt( matcher.group(6) ) : 0;
        int bet = matcher.group(7) == null ? 1 : matcher.group(8).isEmpty() ? 1 : Integer.parseInt( matcher.group(8) );

        return new int[] {maj, min, rev, itr, bet};
    }

    public static boolean isNewer(String version, String source) {
        int[] vVer = getNumber(version);
        int[] sVer = getNumber(source);

        for (int i = 0; i < vVer.length; i++) {
            if (vVer[i] != sVer[i]) return vVer[i] > sVer[i];
        }

        return true;
    }
}