/**
 * Configuration Object Class
 * <p>
 * Provides simplified reference to the YamlConfiguration object,
 * including file loading options and a default settings LinkedHashMap.
 */
package uk.co.projectrogue.shared;

import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.io.IOException;
import java.util.LinkedHashMap;

public class Configuration extends YamlConfiguration {
    private File file;
    private String fileName;

    public Configuration(final JavaPlugin plugin, final String fileName, final LinkedHashMap<String, Object> configDefaults, final String name) {
        final String folderPath = plugin.getDataFolder().getAbsoluteFile() + File.separator;
        file = new File(folderPath + fileName);

        this.fileName = fileName;

        if (!file.exists()) {
            if (configDefaults == null) {
                if (plugin.getResource(fileName) != null) {
                    plugin.saveResource(fileName, false);
                    plugin.getLogger().info("New '" + name + "' file copied from JAR!");

                    try {
                        this.load(file);
                    } catch (final Exception e) {
                        e.printStackTrace();
                    }
                }
            } else {
                for (final String key : configDefaults.keySet()) {
                    this.set(key, configDefaults.get(key));
                }

                try {
                    this.save(file);
                    plugin.getLogger().info("New '" + name + "' file created!");
                } catch (final IOException e) {
                    e.printStackTrace();
                }
            }
        } else {
            try {
                this.load(file);
                plugin.getLogger().info("Existing '" + name + "' file loaded.");
            } catch (final Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void load() {
        try {
            this.load(file);
        } catch (final Exception e) {
            e.printStackTrace();
        }
    }

    public void reload() {
        load();
    }

    public void save() {
        try {
            this.save(file);
        } catch (final Exception e) {
            e.printStackTrace();
        }
    }

    public String getFileName() {
        return fileName;
    }
}