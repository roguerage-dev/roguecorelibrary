package uk.co.projectrogue.shared.hooks.json.items;

import org.bukkit.inventory.ItemStack;

public interface JSONItemAPI {
    String getJSONItem(ItemStack itemStack);
}