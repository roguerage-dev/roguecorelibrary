package uk.co.projectrogue.shared.hooks.json.messages;

import org.bukkit.entity.Player;

public interface JSONMessageAPI {
    void sendJSONMessage(Player player, String json);
    void broadcastJSONMessage(String json);
}
