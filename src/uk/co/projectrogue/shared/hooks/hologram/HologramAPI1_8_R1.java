package uk.co.projectrogue.shared.hooks.hologram;

import net.minecraft.server.v1_8_R1.EntityArmorStand;
import net.minecraft.server.v1_8_R1.PacketPlayOutEntityDestroy;
import net.minecraft.server.v1_8_R1.PacketPlayOutSpawnEntityLiving;
import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_8_R1.CraftWorld;
import org.bukkit.craftbukkit.v1_8_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import uk.co.projectrogue.shared.managers.APIManager;
import uk.co.projectrogue.shared.utils.SerializableLocation;

import java.util.ArrayList;
import java.util.List;

public class HologramAPI1_8_R1 implements HologramAPI {
    private final APIManager manager = APIManager.getManager();
    private final JavaPlugin plugin = APIManager.getPlugin();

    private List<EntityArmorStand> entityList;
    private SerializableLocation location;
    private List<String> lines;
    private double lineSpread;

    public HologramAPI1_8_R1(List<String> lines, double lineSpread, Location location) {
        setLines(lines);
        setLocation(location);
        setEntityList(new ArrayList<>());
        setLineSpread(lineSpread);
    }

    public HologramAPI showPlayer(Player player, int displayTime) {
        showPlayer(player);
        plugin.getServer().getScheduler().runTaskLaterAsynchronously(plugin, () -> hidePlayer(player), displayTime);
        return this;
    }


    public HologramAPI showAll(int displayTime) {
        showAll();
        plugin.getServer().getScheduler().runTaskLaterAsynchronously(plugin, this::hideAll, displayTime);
        return this;
    }

    public HologramAPI showPlayer(Player player) {
        for (EntityArmorStand armor : getEntityList()) {
            CraftPlayer craftPlayer = (CraftPlayer) player;

            PacketPlayOutSpawnEntityLiving packet = new PacketPlayOutSpawnEntityLiving(armor);
            craftPlayer.getHandle().playerConnection.sendPacket(packet);
        }

        return this;
    }

    public HologramAPI hidePlayer(Player player) {
        for (EntityArmorStand armor : getEntityList()) {
            CraftPlayer craftPlayer = (CraftPlayer) player;

            PacketPlayOutEntityDestroy packet = new PacketPlayOutEntityDestroy(armor.getId());
            craftPlayer.getHandle().playerConnection.sendPacket(packet);
        }

        return this;
    }

    public HologramAPI showAll() {
        for (Player player : plugin.getServer().getOnlinePlayers()) {
            for (EntityArmorStand armor : getEntityList()) {
                CraftPlayer craftPlayer = (CraftPlayer) player;

                PacketPlayOutSpawnEntityLiving packet = new PacketPlayOutSpawnEntityLiving(armor);
                craftPlayer.getHandle().playerConnection.sendPacket(packet);
            }
        }

        return this;
    }

    public HologramAPI hideAll() {
        for (Player player : plugin.getServer().getOnlinePlayers()) {
            for (EntityArmorStand armor : getEntityList()) {
                CraftPlayer craftPlayer = (CraftPlayer) player;

                PacketPlayOutEntityDestroy packet = new PacketPlayOutEntityDestroy(armor.getId());
                craftPlayer.getHandle().playerConnection.sendPacket(packet);
            }
        }

        return this;
    }

    public HologramAPI create() {
        Location location = getLocation().subtract(0, getLineSpread(), 0).add(0, getLineSpread() * getLines().size(), 0);
        for (String line : getLines()) {
            CraftWorld craftWorld = (CraftWorld) location.getWorld();

            EntityArmorStand entity = new EntityArmorStand(craftWorld.getHandle(), location.getX(), location.getY(), location.getZ());
            entity.setCustomName(manager.color(line));
            entity.setCustomNameVisible(true);
            entity.setInvisible(true);
            entity.setGravity(false);
            getEntityList().add(entity);

            location = location.subtract(0, getLineSpread(), 0);
        }

        return this;
    }

    // Getters & Setters
    public Location getLocation() {
        return location.asBukkitLocation();
    }

    public HologramAPI setLocation(Location location) {
        this.location = new SerializableLocation(location);
        return this;
    }

    public List<String> getLines() {
        return lines;
    }

    public HologramAPI setLines(List<String> lines) {
        this.lines = lines;
        return this;
    }

    public List<EntityArmorStand> getEntityList() {
        return entityList;
    }

    public void setEntityList(List<EntityArmorStand> entityList) {
        this.entityList = entityList;
    }

    public HologramAPI setLineSpread(double lineSpread) {
        this.lineSpread = lineSpread;
        return this;
    }

    public double getLineSpread() {
        return lineSpread;
    }
}