package uk.co.projectrogue.shared.hooks.hologram;

import org.bukkit.Location;
import org.bukkit.entity.Player;

import java.util.List;

public interface HologramAPI {
    HologramAPI create();

    HologramAPI showPlayer(Player player, int display);

    HologramAPI showPlayer(Player player);

    HologramAPI hidePlayer(Player player);

    HologramAPI showAll(int display);

    HologramAPI showAll();

    HologramAPI hideAll();

    Location getLocation();

    HologramAPI setLocation(Location location);

    List<String> getLines();

    HologramAPI setLines(List<String> lines);

    double getLineSpread();

    HologramAPI setLineSpread(double spread);
}
