package uk.co.projectrogue.shared.hooks.title;

import org.bukkit.entity.Player;

public interface TitleAPI {
    void send(Player player, String text, String subtext, int fadein, int display, int fadeout);
    void sendTitle(Player player, String text, int fadein, int display, int fadeout);
    void sendSubTitle(Player player, String text, int fadein, int display, int fadeout);

    void broadcast(String text, String subtext, int fadein, int display, int fadeout);
    void broadcastTitle(String text, int fadein, int display, int fadeout);
    void broadcastSubTitle(String text, int fadein, int display, int fadeout);

    void removeTitle(Player player);
    void resetTitle(Player player);
}
