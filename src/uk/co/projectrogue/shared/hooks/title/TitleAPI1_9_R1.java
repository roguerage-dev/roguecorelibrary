package uk.co.projectrogue.shared.hooks.title;

import net.minecraft.server.v1_9_R1.IChatBaseComponent;
import net.minecraft.server.v1_9_R1.PacketPlayOutTitle;
import net.minecraft.server.v1_9_R1.PacketPlayOutTitle.EnumTitleAction;
import org.bukkit.craftbukkit.v1_9_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;
import uk.co.projectrogue.shared.managers.APIManager;
import org.bukkit.Bukkit;

public class TitleAPI1_9_R1 implements TitleAPI {


    @Override
    public void send(Player player, String text, String subtext, int fadein, int display, int fadeout) {
        sendTitle(player, text, fadein, display, fadeout);
        sendSubTitle(player, subtext, fadein, display, fadeout);
    }

    @Override
    public void sendTitle(Player player, String text, int fadein, int display, int fadeout) {
        String pText = APIManager.getManager().color(text);
        int pFadeIn = 20 * fadein;
        int pDisplayTime = 20 * display;
        int pFadeOut = 20 * fadeout;

        IChatBaseComponent base = IChatBaseComponent.ChatSerializer.a("{\"text\": \"" + pText + "\"}");
        EnumTitleAction action = EnumTitleAction.TITLE;

        PacketPlayOutTitle title = new PacketPlayOutTitle(action, base, pFadeIn, pDisplayTime, pFadeOut);

        CraftPlayer craftPlayer = (CraftPlayer) player;
        craftPlayer.getHandle().playerConnection.sendPacket(title);
    }

    @Override
    public void sendSubTitle(Player player, String text, int fadein, int display, int fadeout) {
        String pText = APIManager.getManager().color(text);
        int pFadeIn = 20 * fadein;
        int pDisplayTime = 20 * display;
        int pFadeOut = 20 * fadeout;

        IChatBaseComponent base = IChatBaseComponent.ChatSerializer.a("{\"text\": \"" + pText + "\"}");
        EnumTitleAction action = EnumTitleAction.SUBTITLE;

        PacketPlayOutTitle title = new PacketPlayOutTitle(action, base, pFadeIn, pDisplayTime, pFadeOut);

        CraftPlayer craftPlayer = (CraftPlayer) player;
        craftPlayer.getHandle().playerConnection.sendPacket(title);
    }

    @Override
    public void broadcast(String text, String subtext, int fadein, int display, int fadeout) {
        for( Player player : APIManager.getOnlinePlayers() ) {
            send(player, text, subtext, fadein, display, fadeout);
        }
    }

    @Override
    public void broadcastTitle(String text, int fadein, int display, int fadeout) {
        for( Player player : APIManager.getOnlinePlayers() ) {
            sendTitle(player, text, fadein, display, fadeout);
        }
    }

    @Override
    public void broadcastSubTitle(String text, int fadein, int display, int fadeout) {
        for( Player player : APIManager.getOnlinePlayers() ) {
            sendSubTitle(player, text, fadein, display, fadeout);
        }
    }

    @Override
    public void removeTitle(Player player) {
        IChatBaseComponent base = IChatBaseComponent.ChatSerializer.a("{\"text\": \"Remove\"}");
        EnumTitleAction action = EnumTitleAction.CLEAR;

        PacketPlayOutTitle title = new PacketPlayOutTitle(action, base, 20, 20, 20);

        CraftPlayer craftPlayer = (CraftPlayer) player;
        craftPlayer.getHandle().playerConnection.sendPacket(title);
    }

    @Override
    public void resetTitle(Player player) {
        IChatBaseComponent base = IChatBaseComponent.ChatSerializer.a("{\"text\": \"Reset\"}");
        EnumTitleAction action = EnumTitleAction.RESET;

        PacketPlayOutTitle title = new PacketPlayOutTitle(action, base, 20, 20, 20);

        CraftPlayer craftPlayer = (CraftPlayer) player;
        craftPlayer.getHandle().playerConnection.sendPacket(title);
    }
}