package uk.co.projectrogue.shared.hooks;

import com.earth2me.essentials.Essentials;
import de.themoep.ShowItem.ShowItem;
import me.clip.placeholderapi.PlaceholderAPIPlugin;
import net.milkbowl.vault.economy.Economy;
import org.bukkit.Material;
import org.bukkit.Server;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;

public class PluginHooks {
    private final JavaPlugin plugin;
    private final Server server;

    protected static PlaceholderAPIPlugin placeholderAPI;
    protected static Economy economy;
    protected static ShowItem showItem;
    protected static Essentials essentials;

    static {
        PluginHooks.placeholderAPI = null;
        PluginHooks.economy = null;
        PluginHooks.showItem = null;
        PluginHooks.essentials = null;
    }

    public PluginHooks(final JavaPlugin plugin) {
        this.plugin = plugin;
        this.server = plugin.getServer();
    }

    private void notify(String message) {
        plugin.getLogger().info("[PluginHooks] " + message);
    }

    public void hookAll() {
        if( this.hookPlaceholderAPI() ) {
            notify("Hooked into PlaceholderAPI");
        }

        if( this.hookEconomy() ) {
            notify("Hooked into Vault");
        }

        if( this.hookShowItem() ) {
            notify("Hooked into ShowItem");
        }

        if( this.hookEssentials() ) {
            notify("Hooked into Essentials");
        }
    }

    private boolean hookEconomy() {
        final String pluginName = "Vault";

        if( server.getPluginManager().getPlugin(pluginName) == null ) {
            return false;
        }

        RegisteredServiceProvider<Economy> provider = server.getServicesManager().getRegistration(Economy.class);
        if( provider == null ) {
            return false;
        }

        PluginHooks.economy = provider.getProvider();
        return PluginHooks.economy != null;
    }

    private boolean hookShowItem() {
        final String pluginName = "ShowItem";

        if( server.getPluginManager().isPluginEnabled(pluginName) ) {
            PluginHooks.showItem = (ShowItem) server.getPluginManager().getPlugin(pluginName);
        }

        return PluginHooks.showItem != null;
    }

    private boolean hookEssentials() {
        final String pluginName = "Essentials";

        if( server.getPluginManager().isPluginEnabled(pluginName) ) {
            PluginHooks.essentials = (Essentials) server.getPluginManager().getPlugin(pluginName);
        }

        ItemStack stack = new ItemStack(Material.LEATHER_BOOTS, 1);
        stack.getType().name();

        return PluginHooks.essentials != null;
    }

    private boolean hookPlaceholderAPI() {
        final String pluginName = "PlaceholderAPI";

        if( server.getPluginManager().isPluginEnabled(pluginName) ) {
            PluginHooks.placeholderAPI = (PlaceholderAPIPlugin) server.getPluginManager().getPlugin(pluginName);
        }

        return PluginHooks.placeholderAPI != null;
    }

    public Economy getEconomy() {
        return PluginHooks.economy;
    }

    public ShowItem getShowItem() {
        return PluginHooks.showItem;
    }

    public Essentials getEssentials() {
        return PluginHooks.essentials;
    }

    public PlaceholderAPIPlugin getPlaceholderAPI() {
        return PluginHooks.placeholderAPI;
    }
}
