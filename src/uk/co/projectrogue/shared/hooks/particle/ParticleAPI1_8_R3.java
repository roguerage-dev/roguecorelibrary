package uk.co.projectrogue.shared.hooks.particle;

import net.minecraft.server.v1_8_R3.EnumParticle;
import net.minecraft.server.v1_8_R3.PacketPlayOutWorldParticles;
import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.Bukkit;
import uk.co.projectrogue.shared.managers.APIManager;

public class ParticleAPI1_8_R3 implements ParticleAPI {


    @Override
    public void sendParticle(Player player, Location location, float x, float y, float z, int speed, String effect, int amount) {
        float locX = (float) location.getX();
        float locY = (float) location.getY();
        float locZ = (float) location.getZ();

        EnumParticle particle = EnumParticle.valueOf(effect);
        PacketPlayOutWorldParticles packet = new PacketPlayOutWorldParticles(particle, false, locX, locY, locZ, x, y, z, speed, amount);

        CraftPlayer craftPlayer = (CraftPlayer) player;
        craftPlayer.getHandle().playerConnection.sendPacket(packet);
    }

    @Override
    public void broadcastParticle(Location location, float x, float y, float z, int speed, String effect, int amount) {
        float locX = (float) location.getX();
        float locY = (float) location.getY();
        float locZ = (float) location.getZ();

        EnumParticle particle = EnumParticle.valueOf(effect);
        PacketPlayOutWorldParticles packet = new PacketPlayOutWorldParticles(particle, false, locX, locY, locZ, x, y, z, speed, amount);

        for( Player player : APIManager.getOnlinePlayers() ) {
            CraftPlayer craftPlayer = (CraftPlayer) player;
            craftPlayer.getHandle().playerConnection.sendPacket(packet);
        }
    }
}
