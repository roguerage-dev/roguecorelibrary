package uk.co.projectrogue.shared.hooks.particle;

import org.bukkit.Location;
import org.bukkit.entity.Player;

public interface ParticleAPI {
    void sendParticle(Player player, Location location, float x, float y, float z, int speed, String effect, int amount);
    void broadcastParticle(Location location, float x, float y, float z, int speed, String effect, int amount);
}
