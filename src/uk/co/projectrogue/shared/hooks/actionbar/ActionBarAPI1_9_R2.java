package uk.co.projectrogue.shared.hooks.actionbar;

import net.minecraft.server.v1_9_R2.IChatBaseComponent;
import net.minecraft.server.v1_9_R2.IChatBaseComponent.ChatSerializer;
import net.minecraft.server.v1_9_R2.PacketPlayOutChat;
import org.bukkit.Bukkit;
import org.bukkit.craftbukkit.v1_9_R2.entity.CraftPlayer;
import org.bukkit.entity.Player;
import uk.co.projectrogue.shared.managers.APIManager;

public class ActionBarAPI1_9_R2 implements ActionBarAPI {

    @Override
    public void sendActionBar(Player player, String message) {
        String pMessage = APIManager.getManager().color(message);

        IChatBaseComponent base = ChatSerializer.a("{\"text\": \"" + pMessage + "\"}");
        PacketPlayOutChat bar = new PacketPlayOutChat(base, (byte) 2);

        CraftPlayer craftPlayer = (CraftPlayer) player;
        craftPlayer.getHandle().playerConnection.sendPacket(bar);
    }

    @Override
    public void broadcastActionBar(String message) {
        for( Player player : APIManager.getOnlinePlayers() ) {
            sendActionBar(player, message);
        }
    }
}
