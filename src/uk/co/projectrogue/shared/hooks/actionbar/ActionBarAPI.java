package uk.co.projectrogue.shared.hooks.actionbar;

import org.bukkit.entity.Player;

public interface ActionBarAPI {
    void sendActionBar(Player player, String message);

    void broadcastActionBar(String message);
}