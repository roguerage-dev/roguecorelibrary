package uk.co.projectrogue.shared.hooks.ping;

import org.bukkit.entity.Player;

public interface PingAPI {
    int getPing(Player player);
}
